# DQRTECH

## CHALLENGE

<https://github.com/smash-gift/fullstack-dev-test>

## TECHNOLOGIES

- Dart/Flutter

## REQUIREMENTS

- flutter >=2.16.2 <3.0.0

## SCREENSHOTS IOS

The ios's screenshots can be seen down below:

## LOGIN

[<img src="./docs/images/ios/login.png">](Login)

## COUNTRIES LIST

[<img src="./docs/images/ios/countries.png">](Countries)

## CITIES LIST

[<img src="./docs/images/ios/cities.png">](Cities)

## GOOGLE MAPS

[<img src="./docs/images/ios/google_maps.png">](Google_Maps)

## SCREENSHOTS ANDROID

The android's screenshots can be seen down below:

## LOGIN

[<img src="./docs/images/android/login.png">](Login)

## COUNTRIES LIST

[<img src="./docs/images/android/countries.png">](Countries)

## CITIES LIST

[<img src="./docs/images/android/cities.png">](Cities)

## GOOGLE MAPS

[<img src="./docs/images/android/google_maps.png">](Google_Maps)

## SERVICES SCREENSHOTS

## GOOGLE ANALYTICS

[<img src="./docs/images/services/google_analytics.png">](GoogleAnalytics)

## GOOGLE ANALYTICS GRAPH

[<img src="./docs/images/services/google_analytics_graph.png">](GoogleAnalyticsGraph)

## GOOGLE CRASHLITYCS

[<img src="./docs/images/services/google_crashlytics.png">](GoogleCrashlytics)

## FIRESTORE

[<img src="./docs/images/services/google_firestore.png">](GoogleFirestore)

## GOOGLE FIREBASH AUTH

[<img src="./docs/images/services/google_firebase_auth.png">](GoogleFirebaseAuth)

## GOOGLE MAPS/GEOCODING

[<img src="./docs/images/services/google_maps.png">](GoogleMaps)

## USAGE

You need to use the folowing credentials to access the app:

```
email: user@email.com
password: 123Change@
```

## INSTALL

Once you have the project in your computer, you just need to install the dependencies:

```
flutter pub get
```

After everything is installed, you can just start the project on your emulator:

```
flutter run
```

## TESTS

```
flutter test
```

## PLAYSTORE

Google Console

[<img src="./docs/images/android/google_console.png">](Google_Console)

Link invite confirmation:

[https://play.google.com/apps/internaltest/4700834941205985263](https://play.google.com/apps/internaltest/4700834941205985263)

Link app on playstore:

[https://play.google.com/store/apps/details?id=com.dqrtech&hl=pt-BR&ah=wKBckDJsj0q2bK3h8XUg5avqfEQ](https://play.google.com/store/apps/details?id=com.dqrtech&hl=pt-BR&ah=wKBckDJsj0q2bK3h8XUg5avqfEQ)

## APP STORE/TESTFLIGHT

Testflight

[<img src="./docs/images/ios/testflight.png">](Testflight)


Test credencials:

```
email: testdqrtech@gmail.com
password: 123Change@
```
